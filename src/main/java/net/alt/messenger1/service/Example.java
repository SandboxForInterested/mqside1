package net.alt.messenger1.service;

import net.alt.messenger1.aop.LogExecutionTime;
import org.springframework.stereotype.Component;

@Component
public class Example {

    @LogExecutionTime
    public void serve() throws InterruptedException {
        Thread.sleep(2000);
    }
}
