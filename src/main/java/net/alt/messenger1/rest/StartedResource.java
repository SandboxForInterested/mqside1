package net.alt.messenger1.rest;

import net.alt.messenger1.aop.LogExecutionTime;
import net.alt.messenger1.service.Example;
import net.alt.messenger1.service.Sender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/hi")
public class StartedResource {

    @Autowired
    private Sender sender;

    @Autowired
    private Example example;

    @GetMapping("/")
    public String getAllCodes() throws InterruptedException {
        example.serve();
        sender.send("selector.q","hi");
        return "ok";
    }
}


