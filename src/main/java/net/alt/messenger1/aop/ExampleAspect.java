package net.alt.messenger1.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ExampleAspect {
//
//    @Around("@annotation(LogExecutionTime)")
//    public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
//        long start = System.currentTimeMillis();
//
//        Object proceed = joinPoint.proceed();
//
//        long executionTime = System.currentTimeMillis() - start;
//
//        System.out.println(joinPoint.getSignature() + " executed in " + executionTime + "ms");
//        return proceed;
//    }
//
//    @Before(value = "execution(* net.alt.messenger1.rest.StartedResource.*(..))")
//    public void beforeAdvice(JoinPoint joinPoint) {
//        System.out.println("Before method:" + joinPoint.getSignature());
//
//    }
//
//    @After(value = "execution(* net.alt.messenger1.rest.StartedResource.*(..))")
//    public void afterAdvice(JoinPoint joinPoint) {
//        System.out.println("After method:" + joinPoint.getSignature());
//
//    }

    @Pointcut("within(net.alt.messenger1.service.*)")
    public void beanAnnotatedWithComponentOrASpecializationOfIt() {}

    // With args(integer).
    @Around("beanAnnotatedWithComponentOrASpecializationOfIt()")
    public void afterReturningFromConstructorInSpringBeanWithIntegerParameter(
            JoinPoint joinPoint) {
        System.out.println("Executed @Injected constructor: "
                + joinPoint.getSignature() + " with the integer: " + 1);

    }

}
