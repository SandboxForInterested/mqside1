package net.alt.messenger1.service;

import net.alt.messenger1.aop.LogExecutionTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@Service
public class Sender {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(Sender.class);

    private final JmsTemplate jmsTemplate;

    public Sender(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

//    @LogExecutionTime
    public void send(String destination, String message) {
        LOGGER.info("sending message='{}' to destination='{}'", message,
                destination);
        jmsTemplate.convertAndSend(destination, message);
    }
}
